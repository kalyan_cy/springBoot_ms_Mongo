package com.fe.restServices.userservice.Services;


import static org.assertj.core.api.Assertions.*;

import com.fe.restServices.userservice.contracts.Address;
import com.fe.restServices.userservice.contracts.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.UUID;

/**
 * Created by kalyan on 6/4/17.
 */


@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeServiceTest {

    @Autowired
    EmployeeServiceCustom userService;
    User e1 , e2 ;

    @Before
    public void setUp() {

        userService.deleteAll();

        Address a1 = new Address();
        a1.setCity("McPherson");
        a1.setState("KS");
        a1.setStreet("N Main");
        a1.setZip("67460");
        a1.setHouseNumber("1721");

        Address a2 = new Address();
        a2.setCity("Pitts");
        a2.setState("PA");
        a2.setStreet("Royal Creek");
        a2.setZip("17220");
        a2.setHouseNumber("1125");

        Address a3 = new Address();
        a3.setCity("Albany");
        a3.setState("NY");
        a3.setStreet("Main st");
        a3.setZip("20220");
        a3.setHouseNumber("555");

        User e = new User(UUID.randomUUID(), "Ryan" , "green",
                1234l , "TX12345" , "L1 Eng" );
        e.setAddress(a1);
        e = userService.save(e);


        e1 = new User(UUID.randomUUID(), "Smith" , "blue",
                1235l , "TX12345" , "L1 Eng" );


        e1.setAddress(a2);
        e2 = new User(UUID.randomUUID(), "Test" , "yellow",
                1235l , "TX12389" , "L1 Eng" );

        e2.setAddress(a3);

        e1 = userService.save(e1);
        e2 = userService.save(e2);

    }

    @Test
    public void findAllEmployees() throws Exception {

        List<User> employees = userService.findAll();
        assertThat(employees).hasSize(3);
    }

    @Test
    public void updateEmployee() throws Exception {

         e2.setJobTitle("L4 eng");
        userService.save(e2);

        e2 = userService.findOne(e2.getId());
        assertThat(e2).extracting("jobTitle").contains("L4 eng");
    }


}