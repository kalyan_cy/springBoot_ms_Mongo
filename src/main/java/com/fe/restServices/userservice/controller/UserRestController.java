package com.fe.restServices.userservice.controller;

import com.fe.restServices.userservice.Services.EmployeeServiceCustom;
import com.fe.restServices.userservice.Utils.LogUtils;
import com.fe.restServices.userservice.contracts.User;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.UUID;

/**
 * Created by kalyan on 6/3/17.
 */

@RestController
@RequestMapping(value = "/userscv", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserRestController {


    @Autowired
    private EmployeeServiceCustom userService;

    private LogUtils log = new LogUtils(LoggerFactory.getLogger(this.getClass()));
    //-------------------Retrieve All Employees--------------------------------------------------------

    @RequestMapping(value = "/employees", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<User>> listAllEmployees() {
        List<User> employees = userService.findAll();
        if (employees.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }


    //-------------------Retrieve Single User--------------------------------------------------------

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getEmployee(@PathVariable("id") String id) {
        log.info("Fetching User with id %s", id);
        User employee = userService.findOne(UUID.fromString(id));
        if (employee == null) {
            log.info("User with id  %s not found", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }


    //-------------------Create a User--------------------------------------------------------

    @RequestMapping(value = "/employee/", method = RequestMethod.POST)
    public ResponseEntity<Void> createEmployee(@RequestBody User employee, UriComponentsBuilder ucBuilder) {
        log.info("Creating User %s", employee.getName());

        if (employee.getId() == null) {
            employee.setId(UUID.randomUUID());
        } else if (userService.findEmployeeById(employee)) {
            log.info("A User with ID %s already exist", employee.getId().toString());
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        userService.save(employee);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/employee/{id}").buildAndExpand(employee.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }


    //------------------- Update a User --------------------------------------------------------

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateEmployee(@PathVariable("id") String id, @RequestBody User employee) {
        log.info("Updating User %s", id);

        if (!id.equals(employee.getId().toString())) {
            log.info("User with id %s and body are not same Bad request", id);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        User currentEmployee = userService.findOne(UUID.fromString(id));

        if (currentEmployee == null) {
            log.info("User with id %s not found", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        currentEmployee.setName(employee.getName());
        currentEmployee.setJobTitle(employee.getJobTitle());
        currentEmployee.setTeam(employee.getTeam());

        userService.save(currentEmployee);
        return new ResponseEntity<>(currentEmployee, HttpStatus.OK);
    }

    //------------------- Delete a User --------------------------------------------------------

    @RequestMapping(value = "/employee/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteEmployee(@PathVariable("id") UUID id) {
        log.info("Fetching & Deleting User with id ", id);

        User employee = userService.findOne(id);
        if (employee == null) {
            log.info("Unable to delete. User with id %s not found", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        userService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
