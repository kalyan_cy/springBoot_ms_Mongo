package com.fe.restServices.userservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;

/**
 * Created by kalyan on 6/3/17.
 */

/*@ComponentScan("com.fe.restServices.*")*/
@Configuration
public class RestSvcConfig {

    @Bean
    public CorsFilter corsFilter() {
        String[] allowedMethods = {"DELETE", "GET", "OPTIONS", "POST", "PUT"};
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.setAllowedMethods(Arrays.asList(allowedMethods));
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
