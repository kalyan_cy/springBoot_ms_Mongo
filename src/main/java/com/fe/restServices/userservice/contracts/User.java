package com.fe.restServices.userservice.contracts;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.UUID;

/**
 * Created by kalyan on 6/1/17.
 */

@Document(collection = "users")
public class User extends ResourceBaseBean {

    private Address address;

    private String team; // enum

    private Long payRoll ;

    private String taxId ;

    private String jobTitle ;

    private String joinDate;


    public User() {
    }

    public User(UUID id, String name, String team, Long payRoll , String taxId , String jobTitle) {

        this.name = name;
        this.team = team;
        this.jobTitle = jobTitle;
        this.payRoll = payRoll;
        this.taxId = taxId;
        super.id = id;

    }


    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getTaxId() {
        return taxId;
    }

    public void setTaxId(String taxId) {
        this.taxId = taxId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(String joinDate) {
        this.joinDate = joinDate;
    }

    public Long getPayRoll() {
        return payRoll;
    }

    public void setPayRoll(Long payRoll) {
        this.payRoll = payRoll;
    }
}
