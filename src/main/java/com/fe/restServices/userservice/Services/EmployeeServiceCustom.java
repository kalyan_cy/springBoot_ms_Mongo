package com.fe.restServices.userservice.Services;

import com.fe.restServices.userservice.contracts.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * Created by kalyan on 6/3/17.
 */

@Service("userService")
public interface EmployeeServiceCustom extends MongoRepository<User, UUID> {


    boolean findEmployeeById(final User employee);

}
