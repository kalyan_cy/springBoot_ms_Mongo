package com.fe.restServices.userservice.Utils;

import org.slf4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by kalyan on 9/4/17.
 */

public class LogUtils {

    private Logger log;

    public LogUtils(Logger log) {
        this.log = log;
    }

    public void debug(String message, Object... arguments) {

        if (log.isDebugEnabled() && message != null) {
            log.debug(String.format(message, arguments));
        }
    }

    public void info(String message, Object... arguments) {

        if (log.isInfoEnabled() && message != null) {
            log.info(String.format(message, arguments));
        }
    }

    public void error(Exception e, String message, Object... arguments) {

        StringWriter writer = new StringWriter();
        e.printStackTrace(new PrintWriter(writer));
        if (log.isInfoEnabled() && message != null) {
            log.error(String.format(message, arguments), writer.toString());
        }
    }
}
