> ## SpringBoot MS MongoDB

**Description:**

> Sample Rest Project Spring Boot, MongoDB , Swagger, Gradle . 

**How to use**

> * Start Mongo DB service locally 
> * Run Spring boot application from IDE or Terminal
> * Access URl : http://localhost:8090/swagger-ui.html